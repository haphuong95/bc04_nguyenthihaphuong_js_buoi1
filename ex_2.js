/**
 * Input:
 * só thực 1 người dùng nhập vào: ví dụ 24
 * só thực 2 người dùng nhập vào: ví dụ 135
 * só thực 3 người dùng nhập vào: ví dụ 0
 * só thực 4 người dùng nhập vào: ví dụ -73
 * só thực 5 người dùng nhập vào: ví dụ -41
 *
 * Todo:
 * tạo biến cho số thực 1,số thực 2,số thực 3,số thực 4,số thực 5 lần lượt là: number1, number2, number3, number4, number5
 * tạo biến giá trị trung bình của 5 số thực này: result
 * gán giá trị cho number1, number2, number3, number4, number5 lần lượt là: 24, 135, 0, -73, -41
 * tính result theo công thức: (number1 + number2 + number3 + number4 + number5) / 5
 * in result ra console
 *
 * Output:
 * giá trị trung bình của 5 số thực:  (24 + 135 + 0 + (-73) + (-41)) / 5 = 9
 */

var number1 = 24;
var number2 = 135;
var number3 = 0;
var number4 = -73;
var number5 = -41;
var result;
result = (number1 + number2 + number3 + number4 + number5) / 5;
console.log("Giá trị trung bình: ", result);
