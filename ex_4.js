/**
 * Input:
 * chiều dài HCN người dùng nhập: ví dụ 40 cm
 * chiều rộng HCN người dùng nhập: ví dụ 20 cm
 *
 * Todo:
 * tạo biến chiều dài HCN: chieuDai
 * tạo biến chiều rộng HCN: chieuRong
 * tạo biến diện tích HCN: dienTich
 * tạo biến chu vi HCN: chuVi
 * gán giá trị cho chieuDai : 40 cm
 * gán giá trị cho chieuRong : 20 cm
 * tính dienTich theo công thức: chieuDai*chieuRong
 * tính chuVi theo công thức: (chieuDai+chieuRong)*2
 * in kết quả dienTich, chuVi ra console
 *
 * Output:
 * diện tích HCN: 40*20 = 800 cm2
 * chu vi HCN: (40+20)*2 = 120 cm
 */

var chieuDai = 40;
var chieuRong = 20;
var dienTich;
var chuVi;
dienTich = chieuDai * chieuRong;
chuVi = (chieuDai + chieuRong) * 2;
console.log("Diện tích HCN: ", dienTich);
console.log("Chu vi HCN: ", chuVi);
