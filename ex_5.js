/**
 * Input:
 * số có 2 chữ số người dùng nhập: ví dụ 74
 *
 *
 * Todo:
 * tạo biến cho số có 2 chữ số: number
 * tạo biến hàng đơn vị của number: hangDonVi
 * tạo biến hàng chục của number: hangChuc
 * tạo biến tổng 2 ký số của number : tong
 * gán giá trị cho number : 74
 * tính hangDonVi theo công thức: number % 10
 * tính hangChuc theo công thức: Math.floor(number/10)
 * tính tong theo công thức: hangDonVi + hangChuc
 * in kết quả tong ra console
 *
 * Output:
 * tổng 2 ký số: 7 + 4 = 11
 */

var number = 74;
var hangDonVi = number % 10;
var hangChuc = Math.floor(number / 10);
var tong;
tong = hangChuc + hangDonVi;
console.log("Tổng 2 ký số: ", tong);
