/**
 * Input:
 * giá trị VND của 1 USD: 23500 VND
 * số tiền USD người dùng nhập: ví dụ 35 USD
 *
 * Todo:
 * tạo biến giá trị VND của 1 USD: gia1USD
 * tạo biến số tiền USD: soTienUSD
 * tạo biến số tiền sau quy đổi từ USD sang VND: tienQuyDoi
 * gán giá trị cho gia1USD : 23500 VND
 * gán giá trị cho soTienUSD : 35 USD
 * tính tienQuyDoi theo công thức: gia1USD * soTienUSD
 * in kết quả tienQuyDoi ra console
 *
 * Output:
 * số tiền sau quy đổi từ USD sang VND: 23500*35 = 822500 VND
 */

var gia1USD = 23500;
var soTienUSD = 35;
var tienQuyDoi;
tienQuyDoi = gia1USD * soTienUSD;
console.log("Số tiền sau quy đổi: ", tienQuyDoi);
