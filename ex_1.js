/**
 * Input:
 * lương 1 ngày: 100000 VND
 * số ngày làm nhân viên nhập vào: ví dụ 26 ngày
 *
 * Todo:
 * tạo biến lương 1 ngày: luong1Ngay
 * tạo biến số ngày làm: soNgayLam
 * tạo biến tiền lương: tienLuong
 * gán giá trị cho luong1Ngay : 100000 VND
 * gán giá trị cho soNgayLam : 26 ngày
 * tính tienLuong theo công thức: luong1Ngay*soNgayLam
 * in kết quả tienLuong ra console
 *
 * Output:
 * tiền lương của nhân viên: 100000 * 26 = 2600000 VND
 */

var luong1Ngay = 100000;
var soNgayLam = 26;
var tienLuong;
tienLuong = luong1Ngay * soNgayLam;
console.log("Tiền lương: ", tienLuong);
